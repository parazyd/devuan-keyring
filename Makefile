.POSIX:

# N.B. This makefile is for my personal package maintenance (parazyd)

NAME    = $(shell dpkg-parsechangelog -S Source)
VERSION = $(shell dpkg-parsechangelog -S Version | cut -d- -f1)
REV     = $(shell dpkg-parsechangelog -S Version | cut -d- -f2)

ORIGTGZ = ../$(NAME)_$(VERSION).orig.tar.gz
DEB     = ../$(NAME)_$(VERSION)-$(REV)_all.deb

KEYRINGS = \
	keyrings/devuan-archive-keyring.gpg \
	keyrings/devuan-keyring-2016-archive.gpg \
	keyrings/devuan-keyring-2016-cdimage.gpg \
	keyrings/devuan-keyring-2017-archive.gpg \
	keyrings/devuan-master-keyring.gpg

all install clean:

$(KEYRINGS):

SHA512SUMS.txt.asc: $(KEYRINGS)
	sha512sum keyrings/*.gpg | gpg --clearsign > $@

$(ORIGTGZ):
	git archive --format=tar.gz $(VERSION) . -o $@

$(DEB): $(ORIGTGZ)
	dpkg-buildpackage -d --force-sign

deb: $(DEB)

.PHONY: all install clean deb
