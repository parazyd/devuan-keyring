devuan-keyring
==============

Public keyring for Devuan packages and the Devuan Archive.


Maintenance
-----------

### Adding a key

```
$ gpg --no-default-keyring --keyring=${keyring}.gpg --import < /path/to/key
```

### Removing a key

```
$ gpg --no-default-keyring --keyring=${keyring-valid}.gpg --export $keyid \
	| gpg --no-default-keyring --keyring=${keyring-removed-keys}.gpg --import
$ gpg --no-default-keyring --keyring=${keyring-valid}.gpg \
	--delete-key $keyid
```

### Generating SHA512SUMS.txt.asc

```
$ sha512sum keyrings/*.gpg | gpg --clearsign > SHA512SUMS.txt.asc
```
